module gitlab.com/pureharvest/nabconnect

go 1.12

require (
	cloud.google.com/go v0.75.0
	github.com/google/go-cmp v0.5.4
	github.com/shopspring/decimal v1.2.0
)
