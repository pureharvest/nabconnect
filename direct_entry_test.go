package nabconnect

import (
	"io/ioutil"
	"os"
	"testing"

	"cloud.google.com/go/civil"
	"github.com/google/go-cmp/cmp"
	"github.com/shopspring/decimal"
)

func Test_DirectEntryParse(t *testing.T) {
	f, err := os.Open("testdata/de_file.txt")

	if err != nil {
		t.Fatal(err)
	}

	defer f.Close()

	deFile, err := ParseDirectEntryFile(f)

	if err != nil {
		t.Fatal(err)
	}

	expected := DirectEntryFile{
		Header: DEDescriptiveRecord{
			ReelSequenceNumber:       1,
			UserFinancialInstitution: "NAB",
			UserName:                 "NAB CONNECT SAMPLE DE FILE",
			UserNumber:               "334303",
			FileDescription:          "PAYROLL",
			Date:                     civil.Date{Year: 2014, Month: 5, Day: 1},
		},
		Transactions: []DEDetailRecord{
			{
				BSB:                  "063-210",
				AccountNumber:        "115632463",
				Indicator:            ' ',
				TransactionCode:      "53",
				Amount:               decimal.RequireFromString("730.2300"),
				AccountTitle:         "Beneficiary A",
				LodgementReference:   "720056",
				TraceBSB:             "083-047",
				TraceAccountNumber:   "594481031",
				RemitterName:         "NAB CONNECT",
				TaxWithholdingAmount: decimal.Zero,
			},
			{BSB: "063-001", AccountNumber: "17432", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000540.00"), AccountTitle: "Beneficiary B", LodgementReference: "820226", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "192-901", AccountNumber: "050694566", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000826.79"), AccountTitle: "Beneficiary C", LodgementReference: "820226", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "402-728", AccountNumber: "62439881", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000923.60"), AccountTitle: "Beneficiary D", LodgementReference: "881195", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "138073146", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000901.67"), AccountTitle: "Beneficiary E", LodgementReference: "930671", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "505725012", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000882.48"), AccountTitle: "Beneficiary F", LodgementReference: "950850", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "012-009", AccountNumber: "4101884", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00001126.55"), AccountTitle: "Beneficiary G", LodgementReference: "961566", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "012-009", AccountNumber: "39108", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00001119.70"), AccountTitle: "Beneficiary H", LodgementReference: "760094", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "523979319", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000660.60"), AccountTitle: "Beneficiary I", LodgementReference: "810330", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "555455764", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000327.24"), AccountTitle: "Beneficiary J", LodgementReference: "810330", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "402-728", AccountNumber: "23949281", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000876.46"), AccountTitle: "Beneficiary K", LodgementReference: "820282", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "012-003", AccountNumber: "5638227", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000990.18"), AccountTitle: "Beneficiary L", LodgementReference: "820298", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "121799849", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000100.00"), AccountTitle: "Beneficiary M", LodgementReference: "850993", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "126788849", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000762.74"), AccountTitle: "Beneficiary N", LodgementReference: "850993", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "012-009", AccountNumber: "238737", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000827.55"), AccountTitle: "Beneficiary O", LodgementReference: "891271", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "350010587", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000844.86"), AccountTitle: "Beneficiary P", LodgementReference: "900831", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "062-919", AccountNumber: "2381590", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000714.11"), AccountTitle: "Beneficiary Q", LodgementReference: "800186", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "033-009", AccountNumber: "123816613", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000380.00"), AccountTitle: "Beneficiary R", LodgementReference: "840765", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "145799666", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000542.88"), AccountTitle: "Beneficiary R", LodgementReference: "840765", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-009", AccountNumber: "12494", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000640.00"), AccountTitle: "Beneficiary S", LodgementReference: "851119", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "062-912", AccountNumber: "45613319", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000673.48"), AccountTitle: "Beneficiary T", LodgementReference: "851119", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "300699766", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000778.52"), AccountTitle: "Beneficiary U", LodgementReference: "861115", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "762339568", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000568.32"), AccountTitle: "Beneficiary V", LodgementReference: "880003", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "788761350", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000862.64"), AccountTitle: "Beneficiary W", LodgementReference: "901394", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "062-133", AccountNumber: "924670", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000986.53"), AccountTitle: "Beneficiary X", LodgementReference: "930115", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "032-731", AccountNumber: "491264", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000616.40"), AccountTitle: "Beneficiary Y", LodgementReference: "940484", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "553-056", AccountNumber: "5785401", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000975.01"), AccountTitle: "Beneficiary Z", LodgementReference: "951245", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "062-009", AccountNumber: "41432", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000200.00"), AccountTitle: "Beneficiary 1", LodgementReference: "820045", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "787556622", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000618.39"), AccountTitle: "Beneficiary 2", LodgementReference: "820045", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "014-009", AccountNumber: "6765180", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00001027.98"), AccountTitle: "Beneficiary 3", LodgementReference: "850165", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "3063", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00001018.77"), AccountTitle: "Beneficiary 4", LodgementReference: "740170", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "402-728", AccountNumber: "73082382", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000909.13"), AccountTitle: "Beneficiary 5", LodgementReference: "700079", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "5588988", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00001100.67"), AccountTitle: "Beneficiary 6", LodgementReference: "610039", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "586421454", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000458.26"), AccountTitle: "Beneficiary 7", LodgementReference: "610039", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "484-009", AccountNumber: "545324", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000635.37"), AccountTitle: "Beneficiary 8", LodgementReference: "860780", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "484-009", AccountNumber: "542206", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000200.00"), AccountTitle: "Beneficiary 9", LodgementReference: "880468", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "4495206", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000723.21"), AccountTitle: "Beneficiary 10", LodgementReference: "880468", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "766-125", AccountNumber: "845183", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000747.51"), AccountTitle: "Beneficiary 11", LodgementReference: "890253", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "74781", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000570.70"), AccountTitle: "Beneficiary 12", LodgementReference: "740028", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "4941", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000854.80"), AccountTitle: "Beneficiary 13", LodgementReference: "740028", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "033-009", AccountNumber: "49463", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000874.47"), AccountTitle: "Beneficiary 14", LodgementReference: "911017", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "544416768", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000631.44"), AccountTitle: "Beneficiary 15", LodgementReference: "911494", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "38325", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000780.00"), AccountTitle: "Beneficiary 16", LodgementReference: "860146", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "192-901", AccountNumber: "143473475", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000740.78"), AccountTitle: "Beneficiary 17", LodgementReference: "860146", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "063-210", AccountNumber: "138595072", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000808.65"), AccountTitle: "Beneficiary 18", LodgementReference: "930846", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "553-009", AccountNumber: "553819", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000658.53"), AccountTitle: "Beneficiary 19", LodgementReference: "931059", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "012-009", AccountNumber: "155946405", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000700.63"), AccountTitle: "Beneficiary 20", LodgementReference: "931170", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "062-905", AccountNumber: "165596512", Indicator: ' ', TransactionCode: "53", Amount: decimal.RequireFromString("00000658.08"), AccountTitle: "Beneficiary 21", LodgementReference: "670066", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
			{BSB: "083-047", AccountNumber: "594481031", Indicator: ' ', TransactionCode: "13", Amount: decimal.RequireFromString("00035095.91"), AccountTitle: "NAB CONNECT DE FILE", LodgementReference: "CR DE EXAMPLE", TraceBSB: "083-047", TraceAccountNumber: "594481031", RemitterName: "NAB CONNECT", TaxWithholdingAmount: decimal.Zero},
		},
		Total: DEFileTotalRecord{
			FileNetTotal:          decimal.RequireFromString("0.00"),
			FileCreditTotal:       decimal.RequireFromString("35095.9100"),
			FileDebitTotal:        decimal.RequireFromString("35095.9100"),
			FileDetailRecordCount: 49,
		},
	}

	if diff := cmp.Diff(expected, deFile); diff != "" {
		t.Fatal(diff)
	}
}

func Test_DirectEntryOutput(t *testing.T) {
	f, err := os.Open("testdata/de_file.txt")

	if err != nil {
		t.Fatal(err)
	}

	defer f.Close()

	deFile, err := ParseDirectEntryFile(f)

	if err != nil {
		t.Fatal(err)
	}

	f.Seek(0, 0)

	contents, err := ioutil.ReadAll(f)

	if err != nil {
		t.Fatal(err)
	}

	if diff := cmp.Diff(string(contents), deFile.String()); diff != "" {
		t.Fatal(diff)
	}
}

func Test_DEDescriptiveRecord(t *testing.T) {

	record := DEDescriptiveRecord{
		ReelSequenceNumber:       1,
		UserFinancialInstitution: "NAB",
		UserName:                 "NAB CONNECT SAMPLE DE FILE",
		UserNumber:               "334303",
		FileDescription:          "PAYROLL",
		Date:                     civil.Date{Year: 2014, Month: 5, Day: 1},
	}

	expected := "0                 01NAB       NAB CONNECT SAMPLE DE FILE334303PAYROLL     010514                                        "

	if record.String() != expected {
		t.Fatalf("expected \n'%s' to equal \n'%s'", record.String(), expected)
	}
}

func Test_DEDetailRecord(t *testing.T) {

	record := DEDetailRecord{
		BSB:                  "063-210",
		AccountNumber:        "115632463",
		Indicator:            ' ',
		TransactionCode:      "53",
		Amount:               decimal.RequireFromString("730.2300"),
		AccountTitle:         "Beneficiary A",
		LodgementReference:   "720056",
		TraceBSB:             "083-047",
		TraceAccountNumber:   "594481031",
		RemitterName:         "NAB CONNECT",
		TaxWithholdingAmount: decimal.Zero,
	}

	expected := "1063-210115632463 530000073023Beneficiary A                   720056            083-047594481031NAB CONNECT     00000000"

	if record.String() != expected {
		t.Fatalf("expected \n'%s' to equal \n'%s'", record.String(), expected)
	}
}

func Test_DEFileTotalRecord(t *testing.T) {

	record := DEFileTotalRecord{
		FileNetTotal:          decimal.RequireFromString("0.00"),
		FileCreditTotal:       decimal.RequireFromString("35095.9100"),
		FileDebitTotal:        decimal.RequireFromString("35095.9100"),
		FileDetailRecordCount: 49,
	}

	expected := "7999-999            000000000000035095910003509591                        000049                                        "

	if record.String() != expected {
		t.Fatalf("expected \n'%s' to equal \n'%s'", record.String(), expected)
	}
}
