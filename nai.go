package nabconnect

import (
	"errors"
	"time"

	"github.com/shopspring/decimal"
)

type AccountSummaryCode string

const (
	CustomerNumber                AccountSummaryCode = "001"
	NumberOfSegmentsForAccount                       = "003"
	OpeningBalance                                   = "010"
	ClosingBalance                                   = "015"
	TotalCredits                                     = "100"
	NumberOfCreditTransactions                       = "102"
	TotalDebits                                      = "400"
	NumberOfDebitTransactions                        = "402"
	AccruedUnpostedCreditInterest                    = "500"
	AccruedUnpostedDebitInterest                     = "501"
	AccountLimit                                     = "502"
	AvailableLimit                                   = "503"
	EffectiveDebitInterestRate                       = "965"
	EffectiveCreditInterestRate                      = "966"
	AccruedStateGovernmentDuty                       = "967"
	AccruedGovernmentCreditTax                       = "968"
	AccruedGovernmentDebitTax                        = "969"
)

type TransactionCode string

const (
	Deposit                                     TransactionCode = "108"
	Cheques                                                     = "175" // Cash/Cheques
	TransferCredits                                             = "195" // Transfer
	Dividend                                                    = "238" // Dividend
	ReversalEntry                                               = "252" // Reversal
	InterestPaid                                                = "305"
	CreditAdjustment                                            = "357" // Adjustment
	Salary                                                      = "373"
	MiscellaneousCredits                                        = "399" // Miscellaneous credit
	ChequesPaid                                                 = "475" // All serial numbers
	TransferDebits                                              = "495" // Transfer
	AutomaticDrawings                                           = "501" // Company’s name (abbreviated)
	DocumentaryLCDrawingsOrFees                                 = "512" // Documentary L/C
	ReversalDebit                                               = "552"
	DishonouredCheques                                          = "555" // Dishonoured cheques
	LoanFees                                                    = "564" // Loan fee
	FlexiPay                                                    = "595" // Merchant name
	DebitAdjustment                                             = "631" // Adjustment
	DebitInterest                                               = "654" // Interest
	MiscellaneousDebits                                         = "699" // Miscellaneous debit
	CreditInterest                                              = "905" // Interest
	NABNomineesCredits                                          = "906" // NAB nominees
	Cash                                                        = "910" // Cash
	CashOrCheques                                               = "911" // Cash/cheques
	AgentCredits1                                               = "915" // Agent number advised
	InterBankCredits                                            = "920" // Company’s name (abbreviated)
	Pension                                                     = "921"
	EFTPOSTransaction                                           = "922"
	FamilyAllowance                                             = "923"
	AgentCredits2                                               = "924"
	BankcardCredits                                             = "925" // Bankcard
	CreditBalanceTransfer                                       = "930" // Balance transfer
	CreditsSummarised                                           = "935" // Not applicable
	EFTPOS                                                      = "936" // Merchant name
	NABForeignCurrencyAccountCreditTransactions                 = "938" // Not applicable
	LoanEstablishmentFees                                       = "950" // Establishment fee
	AccountKeepingFees                                          = "951" // Account keeping fee
	UnusedLimitFees                                             = "952" // Unused limit fee
	SecurityFees                                                = "953" // Security fee
	Charges                                                     = "955" // Charge (or description)
	NABNomineeDebits                                            = "956" // NAB nominees
	StampDutyChequeBook                                         = "960" // Cheque book
	StampDuty                                                   = "961" // Stamp duty
	StampDutySecurity                                           = "962" // Security stamp duty
	EFTPOSDebit                                                 = "963"
	CreditCardCashAdvance                                       = "964"
	StateGovernmentTax                                          = "970" // State government credit tax
	FederalGovernmentTax                                        = "971" // Federal government debit tax
	CreditCardPurchase                                          = "972"
	Bankcard                                                    = "975" //Bankcard
	DebitBalanceTransfers                                       = "980" // Balance transfers
	DebitsSummarised                                            = "985" // Not applicable
	ChequesSummarised                                           = "986" // Not applicable
	NonChequesSummarised                                        = "987" // Not applicable
	NABForeignCurrencyAccountDebitTransaction                   = "988" // Not applicable
)

var transactionCodeTypeMap = map[TransactionCode]string{
	"108": "CR",
	"175": "CR",
	"195": "CR",
	"238": "CR",
	"252": "CR",
	"305": "CR",
	"357": "CR",
	"373": "CR",
	"399": "CR",
	"475": "DR",
	"495": "DR",
	"501": "DR",
	"512": "DR",
	"552": "DR",
	"555": "DR",
	"564": "DR",
	"595": "DR",
	"631": "DR",
	"654": "DR",
	"699": "DR",
	"905": "CR",
	"906": "CR",
	"910": "CR",
	"911": "CR",
	"915": "CR",
	"920": "CR",
	"921": "CR",
	"922": "CR",
	"923": "CR",
	"924": "CR",
	"925": "CR",
	"930": "CR",
	"935": "CR",
	"936": "CR",
	"938": "CR",
	"950": "DR",
	"951": "DR",
	"952": "DR",
	"953": "DR",
	"955": "DR",
	"956": "DR",
	"960": "DR",
	"961": "DR",
	"962": "DR",
	"963": "DR",
	"964": "DR",
	"970": "DR",
	"971": "DR",
	"972": "DR",
	"975": "DR",
	"980": "DR",
	"985": "DR",
	"986": "DR",
	"987": "DR",
	"988": "DR",
}

type NAIFileHeaderRecord struct {
	SenderIdentification   string
	ReceiverIdentification string
	CreationDate           time.Time
	SequenceNumber         int64
	RecordLength           int64
	BlockingFactor         int64
}

func parseNAIFileHeaderRecord(records []string) (NAIFileHeaderRecord, error) {
	return NAIFileHeaderRecord{}, errors.New("not implemented")
}

type NAIGroupHeaderRecord struct {
	UltimateReceiverIdentification string
	OriginatorIdentification       string
	GroupStatus                    string
	AsOfDate                       time.Time
}

func parseNAIGroupHeaderRecord(records []string) (NAIGroupHeaderRecord, error) {
	return NAIGroupHeaderRecord{}, errors.New("not implemented")
}

type NAIAccountIdentifierAndSummaryRecord struct {
	CommercialAccountNumber string
	CurrencyCode            string
	TransactionCodes        string
	Amounts                 decimal.Decimal
}

func parseNAIAccountIdentifierAndSummaryRecord(records []string) (NAIAccountIdentifierAndSummaryRecord, error) {
	return NAIAccountIdentifierAndSummaryRecord{}, errors.New("not implemented")
}

type NAITransactionDetailRecord struct {
	TransactionCode string
	Amount          decimal.Decimal
	FundsType       string
	ReferenceNumber string
	Text            string
}

func parseNAITransactionDetailRecord(records []string) (NAITransactionDetailRecord, error) {
	return NAITransactionDetailRecord{}, errors.New("not implemented")
}

type NAIAccountTrailerRecord struct {
	AccountControlTotalA decimal.Decimal
	AccountControlTotalB decimal.Decimal
}

func parseNAIAccountTrailerRecord(records []string) (NAIAccountTrailerRecord, error) {
	return NAIAccountTrailerRecord{}, errors.New("not implemented")
}

type NAIContinuationRecord struct {
	Remainder string
}

type NAIGroupTrailerRecord struct {
	GroupControlTotalA decimal.Decimal
	NumberOfAccounts   int64
	GroupControlTotalB decimal.Decimal
}

func parseNAIGroupTrailerRecord(records []string) (NAIGroupTrailerRecord, error) {
	return NAIGroupTrailerRecord{}, errors.New("not implemented")
}

type NAIFileTrailerRecord struct {
	FileControlTotalA decimal.Decimal
	NumberOfGroups    int64
	NumberOfRecords   int64
	FileControlTotalB decimal.Decimal
}

func parseNAIFileTrailerRecord(records []string) (NAIFileTrailerRecord, error) {
	return NAIFileTrailerRecord{}, errors.New("not implemented")
}
