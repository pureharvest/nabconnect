package nabconnect

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"

	"cloud.google.com/go/civil"
	"github.com/shopspring/decimal"
)

type DEDescriptiveRecord struct {
	ReelSequenceNumber       uint64
	UserFinancialInstitution string
	UserName                 string
	UserNumber               string
	FileDescription          string
	Date                     civil.Date
}

func (r DEDescriptiveRecord) String() string {
	return fmt.Sprintf(
		"0%17s%02d%3s%7s%-26s%06s%-12s%02d%02d%0s%40s",
		"",
		r.ReelSequenceNumber,
		r.UserFinancialInstitution,
		"",
		r.UserName,
		r.UserNumber,
		r.FileDescription,
		r.Date.Day,
		r.Date.Month,
		strconv.FormatInt(int64(r.Date.Year), 10)[2:],
		"",
	)
}

// ParseDEDescriptiveRecord parses a string without a trailing CR/LF into a Direct Entry Discriptive Record
// The first character must be '0' and the record length must be 120 characters long.
func ParseDEDescriptiveRecord(input string) (DEDescriptiveRecord, error) {
	if len(input) != 120 {
		return DEDescriptiveRecord{}, errors.New("invalid record length")
	}

	if input[0] != '0' {
		return DEDescriptiveRecord{}, errors.New("invalid record type")
	}

	sequenceNumber, err := strconv.ParseUint(input[18:20], 10, 64)

	if err != nil {
		return DEDescriptiveRecord{}, fmt.Errorf("invalid sequence number: %s", input[18:20])
	}

	if _, err := strconv.ParseUint(input[56:62], 10, 64); err != nil {
		return DEDescriptiveRecord{}, fmt.Errorf("invalid user number: %s", input[56:62])
	}

	date, err := time.Parse("020106", input[74:80])

	if err != nil {
		return DEDescriptiveRecord{}, errors.New("invalid date")
	}

	return DEDescriptiveRecord{
		ReelSequenceNumber:       sequenceNumber,
		UserFinancialInstitution: input[20:23],
		UserName:                 strings.TrimRight(input[30:56], " "),
		UserNumber:               strings.TrimLeft(input[56:62], "0"),
		FileDescription:          strings.TrimRight(input[62:74], " "),
		Date:                     civil.DateOf(date),
	}, nil
}

type DEDetailRecord struct {
	BSB                  string
	AccountNumber        string
	Indicator            byte
	TransactionCode      string
	Amount               decimal.Decimal
	AccountTitle         string
	LodgementReference   string
	TraceBSB             string
	TraceAccountNumber   string
	RemitterName         string
	TaxWithholdingAmount decimal.Decimal
}

func (r DEDetailRecord) String() string {
	return fmt.Sprintf(
		"1%s%9s%s%s%010s%-32s%-18s%s%9s%-16s%08s",
		r.BSB,
		r.AccountNumber,
		string(r.Indicator),
		r.TransactionCode,
		r.Amount.Shift(2).StringFixed(0),
		r.AccountTitle,
		r.LodgementReference,
		r.TraceBSB,
		r.TraceAccountNumber,
		r.RemitterName,
		r.TaxWithholdingAmount.Shift(2).StringFixed(0),
	)
}

// ParseDEDetailRecord parses a string without a trailing CR/LF into a Direct Entry Discriptive Record
// The first character must be '0' and the record length must be 120 characters long.
func ParseDEDetailRecord(input string) (DEDetailRecord, error) {
	if len(input) != 120 {
		return DEDetailRecord{}, errors.New("invalid record length")
	}

	if input[0] != '1' {
		return DEDetailRecord{}, errors.New("invalid record type")
	}

	amount, err := decimal.NewFromString(input[20:30])

	if err != nil {
		return DEDetailRecord{}, fmt.Errorf("invalid amount: %s", input[20:30])
	}

	taxAmount, err := decimal.NewFromString(input[112:])

	if err != nil {
		return DEDetailRecord{}, fmt.Errorf("invalid amount: %s", input[112:])
	}

	return DEDetailRecord{
		BSB:                  input[1:8],
		AccountNumber:        strings.TrimLeft(input[8:17], " "),
		Indicator:            input[17],
		TransactionCode:      input[18:20],
		Amount:               amount.Shift(-2),
		AccountTitle:         strings.TrimRight(input[30:62], " "),
		LodgementReference:   strings.TrimRight(input[62:80], " "),
		TraceBSB:             input[80:87],
		TraceAccountNumber:   input[87:96],
		RemitterName:         strings.TrimRight(input[96:112], " "),
		TaxWithholdingAmount: taxAmount.Shift(-2),
	}, nil
}

type DEFileTotalRecord struct {
	FileNetTotal          decimal.Decimal
	FileCreditTotal       decimal.Decimal
	FileDebitTotal        decimal.Decimal
	FileDetailRecordCount uint64
}

func (r DEFileTotalRecord) String() string {
	return fmt.Sprintf(
		"7%s%12s%010s%010s%010s%24s%06d%40s",
		"999-999",
		"",
		r.FileNetTotal.Shift(2).StringFixed(0),
		r.FileCreditTotal.Shift(2).StringFixed(0),
		r.FileDebitTotal.Shift(2).StringFixed(0),
		"",
		r.FileDetailRecordCount,
		"",
	)
}

// ParseDEFileTotalRecord parses a string without a trailing CR/LF into a Direct Entry Discriptive Record
// The first character must be '0' and the record length must be 120 characters long.
func ParseDEFileTotalRecord(input string) (DEFileTotalRecord, error) {
	if len(input) != 120 {
		return DEFileTotalRecord{}, errors.New("invalid record length")
	}

	if input[0] != '7' {
		return DEFileTotalRecord{}, errors.New("invalid record type")
	}

	if input[1:8] != "999-999" {
		return DEFileTotalRecord{}, errors.New("invalid bsb")
	}

	netTotal, err := decimal.NewFromString(input[20:30])

	if err != nil {
		return DEFileTotalRecord{}, errors.New("invalid net total amount")
	}

	creditTotal, err := decimal.NewFromString(input[30:40])

	if err != nil {
		return DEFileTotalRecord{}, errors.New("invalid credit total amount")
	}

	debitTotal, err := decimal.NewFromString(input[40:50])

	if err != nil {
		return DEFileTotalRecord{}, errors.New("invalid debit total amount")
	}

	recordCount, err := strconv.ParseUint(input[74:80], 10, 64)

	if err != nil {
		return DEFileTotalRecord{}, errors.New("invalid record count")
	}

	return DEFileTotalRecord{
		FileNetTotal:          netTotal.Shift(-2),
		FileCreditTotal:       creditTotal.Shift(-2),
		FileDebitTotal:        debitTotal.Shift(-2),
		FileDetailRecordCount: recordCount,
	}, nil
}

type DirectEntryFile struct {
	Header       DEDescriptiveRecord
	Transactions []DEDetailRecord
	Total        DEFileTotalRecord
}

func ParseDirectEntryFile(input io.Reader) (DirectEntryFile, error) {

	scn := bufio.NewScanner(input)

	if !scn.Scan() {
		return DirectEntryFile{}, scn.Err()
	}

	header, err := ParseDEDescriptiveRecord(scn.Text())

	if err != nil {
		return DirectEntryFile{}, fmt.Errorf("could not read descriptive record: %s", err.Error())
	}

	file := DirectEntryFile{
		Header: header,
	}

	lineNumber := 1
	totalsRead := false

	for scn.Scan() {
		if totalsRead {
			return file, fmt.Errorf("records read after totals at line %d", lineNumber)
		}

		line := scn.Text()

		switch line[0] {
		case '1':
			record, err := ParseDEDetailRecord(line)
			if err != nil {
				return DirectEntryFile{}, fmt.Errorf("could not read detail record at line %d: %s", lineNumber, err.Error())
			}
			file.Transactions = append(file.Transactions, record)
			break
		case '7':
			record, err := ParseDEFileTotalRecord(line)
			if err != nil {
				return DirectEntryFile{}, fmt.Errorf("could not read total record at line %d: %s", lineNumber, err.Error())
			}
			file.Total = record
			break
		}

		lineNumber++

	}

	if scn.Err() != nil {
		return file, fmt.Errorf("error reading file: %s", err.Error())
	}

	return file, nil

}

func (file DirectEntryFile) String() string {
	transactions := make([]string, len(file.Transactions))

	for i, transaction := range file.Transactions {
		transactions[i] = transaction.String()
	}
	return fmt.Sprintf("%s\r\n%s\r\n%s", file.Header, strings.Join(transactions, "\r\n"), file.Total)
}
